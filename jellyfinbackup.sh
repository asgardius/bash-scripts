#!/bin/bash
CURRENTDATE=`date +"%Y-%m-%d-%H-%M"`
sudo systemctl stop jellyfin
sudo borg create ssh://pi@naomi.asgardius.company:22/data/jellyfin::jellyfin-${CURRENTDATE} /var/lib/jellyfin /etc/jellyfin /var/log/jellyfin /var/cache/jellyfin /etc/default/jellyfin /usr/bin/jellyfinbackup
sudo systemctl start jellyfin
