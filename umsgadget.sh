#!/bin/bash

# Konfiguration fuer ein Composite Gadget Device
# mit mehreren HID und Flash Komponenten

#board config anlegen
if [ $# -eq 0 ]; then
    if [ -d "/sys/kernel/config/usb_gadget/multiDeskBoard" ]; then
        echo "" > /sys/kernel/config/usb_gadget/multiDeskBoard/UDC
    fi
fi
if [ $# -eq 1 ]; then
    if [ -d "/sys/kernel/config/usb_gadget/multiDeskBoard" ]; then
#        echo "" > /sys/kernel/config/usb_gadget/multiDeskBoard/UDC
        echo $1 > /sys/kernel/config/usb_gadget/multiDeskBoard/functions/mass_storage.usb1/lun.0/file
        ls /sys/class/udc > /sys/kernel/config/usb_gadget/multiDeskBoard/UDC
    else
        mkdir /sys/kernel/config/usb_gadget/multiDeskBoard
        cd /sys/kernel/config/usb_gadget/multiDeskBoard
        #hardware id setzen
        echo 0x0419 > bcdDevice
        echo 0x0200 > bcdUSB
        echo 0x1d6b > idVendor
        echo 0x0104 > idProduct
        #Geraetetyp setzen
        echo 0x00 > bDeviceClass
        echo 0x00 > bDeviceSubClass
        echo 0x00 > bDeviceProtocol
        echo 0x08 > bMaxPacketSize0
        #klartextbeschreibung setzen
        mkdir strings/0x409
        mkdir strings/0x407
        cd strings/0x409
        echo "RSI_HUD_Solutions" > manufacturer
        echo "MultiDeskBoard" > product
        echo "1337" > serialnumber
        cd ../../
        cd strings/0x407
        echo "RSI_HUD_Solutions" > manufacturer
        echo "MultiDeskBoard" > product
        echo "1337" > serialnumber
        cd ../../
        #geraetefunktion setzen
        mkdir functions/mass_storage.usb1
        #mkdir functions/hid.usb0
        #HID0 Funktion Konfiguration auf keyboard setzen
        #cd functions/hid.usb0
        #echo 1 > protocol
        #echo 8 > report_length
        #echo 1 > subclass
        #cp /home/pi/USBConfigBuffer/mediaKeyboardRepDesc report_desc
        #cd ../../
        #Mass Storage 1 Konfiguration setzen
        cd functions/mass_storage.usb1
        echo 0 > stall
        echo 1 > lun.0/removable
        echo 0 > lun.0/ro
        echo $1 > lun.0/file
        cd ../../
        #Bus Hardware Konfig anlegen und verlinken
        mkdir configs/c.1
        cd configs/c.1
        echo 0x80 > bmAttributes
        echo 100 > MaxPower
        mkdir strings/0x409
        echo "MDB Config1" > strings/0x409/configuration
        mkdir strings/0x407
        echo "MDB Config1" > strings/0x407/configuration
        cd ../../
        ln -s functions/mass_storage.usb1 configs/c.1
        #ln -s functions/hid.usb0 configs/c.1
        #Geraet aktivieren
        ls /sys/class/udc > UDC
    fi
fi
