#!/bin/bash
CURRENTDATE=`date +"%Y-%m-%d-%H-%M"`
sudo systemctl stop plexmediaserver
sudo borg create ssh://pi@naomi.asgardius.company:22/data/plex::plex-${CURRENTDATE} /var/lib/plexmediaserver /usr/bin/plexbackup
sudo systemctl start plexmediaserver