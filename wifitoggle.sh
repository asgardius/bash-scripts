#!/bin/sh

if [ $(nmcli radio wifi | awk '/led/ {print}') = 'enabled'  ] ; then
    nmcli radio wifi off
    echo 'wifi off'
else
    nmcli radio wifi on
#    notify-send -i network-wireless-none "Wireless" "Wireless enabled"
    echo 'wifi on'
fi
