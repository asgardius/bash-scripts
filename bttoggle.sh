#!/bin/sh

if bluetoothctl show | grep 'Powered: yes' ; then
    bluetoothctl power off
    echo 'bluetooth off'
else
    bluetoothctl power on
#    notify-send -i network-wireless-none "Wireless" "Wireless enabled"
    echo 'bluetooth on'
fi
